local level = tonumber(minetest.settings:get("light_level")) or 4
local _seed = minetest.get_mapgen_params().seed

min_light = {}

local set_light = function(minp, maxp, seed)
	local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
	local area = VoxelArea:new{MinEdge=minp, MaxEdge=maxp}
	local data = vm:get_data()
	vm:set_data(data)
	vm:set_lighting({day=level, night=level})
	vm:calc_lighting()
	vm:write_to_map(data)
end

minetest.register_on_generated(set_light)

local light_test_dist = 8

minetest.register_node("min_light_level:glow_air", {
	-- name = "glow_air",
	groups = {not_in_creative_inventory = 1},
	drawtype = "airlike",
	drop = "",
	air_equivalent = true,
	paramtype = "light",
	light_source = level,
	description = "air but light level more than 0",
	inventory_image = "air.png",
	floodable = true,
	pointable = false,
	sunlight_propagates = true,
	walkable = false,
	diggable = false,
	-- type = "node",
	buildable_to = true,
	-- is_ground_content = true,
	-- stack_max = 64,
	-- sounds = mcl_sounds.node_sound_stone_defaults(),
	wield_image = "air.png",
})

function min_light.check_node_light(p)
	local node = minetest.get_node(p)
	if node.name == "air" or node.name == "min_light_level:glow_air" then
		local light = minetest.get_node_light(p, 0.5)
		if (node.name == "air") and light < level then
			minetest.set_node(p, {name = "min_light_level:glow_air"})
		elseif (node.name == "min_light_level:glow_air" and (light > level
		or minetest.get_node_light(vector.offset(p, 0, 1, 0), 0.5) > level)) then
			minetest.set_node(p, {name = "air"})
		end
	end
end

function min_light.set_all_nearby_nodes(pos)
	local d = 8
	for x = -d, d do
		for y = -d, d do
			for z = -d, d do
				p = vector.offset(pos, -x, -y, -z)
				min_light.check_node_light(p)
			end
		end
	end
end

function min_light.check_random_near(pos, dist)
	local p = vector.offset(pos, (math.random()-0.5)*dist, (math.random()-0.5)*dist, (math.random()-0.5)*dist)
	min_light.check_node_light(p)
end


local globalstep_timer = 0
local function light_player_nodes(dtime)
	if globalstep_timer < 1 then
		globalstep_timer = globalstep_timer + dtime
		return
	else
		globalstep_timer = globalstep_timer - 1
	end
	for _, player in pairs(minetest.get_connected_players()) do
		-- if player:get_velocity() ~= vector.new() then
			min_light.set_all_nearby_nodes(player:get_pos())
			-- min_light.check_random_near(player:get_pos(), 8)
		-- end
	end
end

minetest.register_on_mods_loaded(function()
	minetest.register_globalstep(light_player_nodes)
end)

